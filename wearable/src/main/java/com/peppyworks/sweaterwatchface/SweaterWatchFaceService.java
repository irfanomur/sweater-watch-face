/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface;

import com.peppyworks.sweaterwatchface.common.WatchFaceDrawer;
import com.peppyworks.watchfaceconstructor.wearable.PeppyWatchFaceService;

import android.support.wearable.watchface.WatchFaceStyle;
import android.view.Gravity;

import java.util.Calendar;

public class SweaterWatchFaceService extends PeppyWatchFaceService {

    @Override
    public void onCreate() {
        super.onCreate();

        registerWatchFaceDrawer(new WatchFaceDrawer(getApplicationContext()));
    }

    @Override
    protected WatchFaceStyle getWatchFaceStyle() {
        WatchFaceStyle.Builder builder =
                new WatchFaceStyle.Builder(this)
                        .setAmbientPeekMode(WatchFaceStyle.AMBIENT_PEEK_MODE_HIDDEN)
                        .setCardPeekMode(WatchFaceStyle.PEEK_MODE_SHORT)
                        .setAcceptsTapEvents(true)
                        .setPeekOpacityMode(WatchFaceStyle.PEEK_OPACITY_MODE_TRANSLUCENT)
                        .setBackgroundVisibility(WatchFaceStyle.BACKGROUND_VISIBILITY_INTERRUPTIVE)
                        .setHotwordIndicatorGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL)
                        .setStatusBarGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL)
                        .setShowSystemUiTime(false);
        return builder.build();
    }

    @Override
    protected void onCalendarChanged(Calendar oldCalendar, Calendar newCalendar) {
    }

}
