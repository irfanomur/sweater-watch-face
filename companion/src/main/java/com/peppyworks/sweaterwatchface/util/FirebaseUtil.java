/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface.util;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseUtil {
    public static DatabaseReference getBaseRef() {
        return FirebaseDatabase.getInstance().getReference();
    }

    public static DatabaseReference getWatchFacesRef() {
        return getBaseRef().child("watch_faces");
    }

    public static String getWatchFacesPath() {
        return "watch_faces/";
    }

}
