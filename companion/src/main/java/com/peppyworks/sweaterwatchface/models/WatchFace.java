/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface.models;

public class WatchFace {

    private String name;
    private String preview_storage_uri;
    private String preview_url;
    private float price;
    private String package_name;
    private int priority;

    public WatchFace() {
        // empty default constructor, necessary for Firebase to be able to deserialize
    }

    public WatchFace(String name, String preview_storage_uri, String preview_url, float price,
                     String package_name, int priority) {
        this.name = name;
        this.preview_storage_uri = preview_storage_uri;
        this.preview_url = preview_url;
        this.price = price;
        this.package_name = package_name;
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public String getPreview_storage_uri() {
        return preview_storage_uri;
    }

    public String getPreview_url() {
        return preview_url;
    }

    public float getPrice() {
        return price;
    }

    public String getPackage_name() {
        return package_name;
    }

    public int getPriority() {
        return priority;
    }
}
