/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface;

import com.peppyworks.watchfaceconstructor.mobile.util.AnalyticsHelper;

import android.app.Application;

/**
 * {@link Application} used to initialize Analytics. Code initialized in Application
 * classes is rare since this code will be run any time a ContentProvider, Activity, or Service is
 * used by the user or system. Analytics, dependency injection, and multi-dex frameworks are in this
 * very small set of use cases.
 */
public class AppApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialise the Google Analytics Tracker
        AnalyticsHelper.initializeAnalyticsTracker(getApplicationContext());
    }

}
