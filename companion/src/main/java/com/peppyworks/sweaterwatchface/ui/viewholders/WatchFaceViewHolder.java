/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface.ui.viewholders;

import com.peppyworks.sweaterwatchface.R;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class WatchFaceViewHolder extends RecyclerView.ViewHolder {
    public View mView;
    public TextView mTitleView;
    public TextView mPriceView;
    public ImageView mThumbView;

    public WatchFaceViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        mTitleView = (TextView) itemView.findViewById(R.id.title_text);
        mPriceView = (TextView) itemView.findViewById(R.id.price_text);
        mThumbView = (ImageView) itemView.findViewById(R.id.thumbnail);
    }

}