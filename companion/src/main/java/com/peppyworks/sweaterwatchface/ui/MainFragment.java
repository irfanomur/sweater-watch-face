/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface.ui;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import com.bumptech.glide.Glide;
import com.peppyworks.sweaterwatchface.R;
import com.peppyworks.sweaterwatchface.models.WatchFace;
import com.peppyworks.sweaterwatchface.ui.firebase.FirebaseRecyclerAdapter;
import com.peppyworks.sweaterwatchface.ui.viewholders.WatchFaceViewHolder;
import com.peppyworks.sweaterwatchface.ui.widget.DividerItemDecoration;
import com.peppyworks.sweaterwatchface.util.FirebaseUtil;
import com.peppyworks.watchfaceconstructor.common.util.TypefaceSpan;
import com.peppyworks.watchfaceconstructor.mobile.util.AnalyticsHelper;
import com.peppyworks.watchfaceconstructor.mobile.util.Intents;

import android.app.Fragment;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class MainFragment extends Fragment implements View.OnClickListener {
    private static final String DOLLAR_SIGN = "$";

    private Typeface mGothamBookTypeface;
    private Typeface mGothamMediumTypeface;
    private Typeface mGothamBoldTypeface;

    private RecyclerView mRecyclerView;
    private LinearLayout mDownloadMoreLayout;
    private View mDownloadMoreDivider;

    private RecyclerView.Adapter<WatchFaceViewHolder> mAdapter;

    public MainFragment() {
    }

    /**
     * Create a new instance of this fragment.
     */
    public static MainFragment newInstance(Typeface gothamBook,
                                           Typeface gothamMedium,
                                           Typeface gothamBold) {
        MainFragment fragment = new MainFragment();
        fragment.setGothamBookTypeface(gothamBook);
        fragment.setGothamMediumTypeface(gothamMedium);
        fragment.setGothamBoldTypeface(gothamBold);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.watch_face_recycler_view);
        mDownloadMoreLayout = (LinearLayout) rootView.findViewById(R.id.download_more_layout);
        mDownloadMoreDivider = rootView.findViewById(R.id.download_more_divider);

        TextView customizeText = (TextView) rootView.findViewById(R.id.customize_text);
        customizeText.setTypeface(mGothamMediumTypeface);
        TextView rateText = (TextView) rootView.findViewById(R.id.rate_text);
        rateText.setTypeface(mGothamMediumTypeface);
        TextView downloadMoreText = (TextView) rootView.findViewById(R.id.download_more_text);
        downloadMoreText.setTypeface(mGothamMediumTypeface);
        TextView facebookText = (TextView) rootView.findViewById(R.id.facebook_text);
        facebookText.setTypeface(mGothamMediumTypeface);
        TextView instagramText = (TextView) rootView.findViewById(R.id.instagram_text);
        instagramText.setTypeface(mGothamMediumTypeface);
        TextView likeText = (TextView) rootView.findViewById(R.id.like_text);
        likeText.setTypeface(mGothamBookTypeface);
        TextView followText = (TextView) rootView.findViewById(R.id.follow_text);
        followText.setTypeface(mGothamBookTypeface);
        TextView contactText = (TextView) rootView.findViewById(R.id.contact_text);
        contactText.setTypeface(mGothamMediumTypeface);
        TextView sendText = (TextView) rootView.findViewById(R.id.send_text);
        sendText.setTypeface(mGothamBookTypeface);

        rootView.findViewById(R.id.customize_layout).setOnClickListener(this);
        rootView.findViewById(R.id.rate_layout).setOnClickListener(this);
        rootView.findViewById(R.id.facebook_layout).setOnClickListener(this);
        rootView.findViewById(R.id.instagram_layout).setOnClickListener(this);
        rootView.findViewById(R.id.contact_layout).setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        mRecyclerView.addItemDecoration(itemDecoration);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mRecyclerView.setLayoutManager(linearLayoutManager);

        ConnectivityManager cm = (ConnectivityManager)
                getActivity().getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork == null || !activeNetwork.isConnected()) {
            showMore(false);
        }

        DatabaseReference connectedRef =
                FirebaseDatabase.getInstance().getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    showMore(true);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });

        Query allWatchFacesQuery = FirebaseUtil.getWatchFacesRef().orderByChild("priority");
        allWatchFacesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                    showMore(true);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                showMore(false);
            }
        });
        allWatchFacesQuery.keepSynced(true);

        mAdapter = getPeppyRecyclerAdapter(allWatchFacesQuery);

        mRecyclerView.setAdapter(mAdapter);
    }

    private FirebaseRecyclerAdapter<WatchFace, WatchFaceViewHolder>
    getPeppyRecyclerAdapter(Query query) {
        return new FirebaseRecyclerAdapter<WatchFace, WatchFaceViewHolder>(
                WatchFace.class, R.layout.watch_face_item, WatchFaceViewHolder.class, query) {

            @Override
            public boolean filter(WatchFace watchFace) {
                return Intents.isAppInstalled(getActivity(), watchFace.getPackage_name());
            }

            @Override
            public void populateViewHolder(final WatchFaceViewHolder watchFaceViewHolder,
                                           final WatchFace watchFace, final int position) {
                setupWatchFace(watchFaceViewHolder, watchFace);
            }

            @Override
            public void onViewRecycled(WatchFaceViewHolder holder) {
                super.onViewRecycled(holder);
                Glide.clear(holder.mThumbView);
            }

        };
    }

    private void setupWatchFace(final WatchFaceViewHolder viewHolder, final WatchFace watchFace) {
        Glide.with(this)
                .load(watchFace.getPreview_url())
                .placeholder(R.drawable.watch_face_placeholder)
                .into(viewHolder.mThumbView);

        viewHolder.mTitleView.setText(watchFace.getName());
        viewHolder.mTitleView.setTypeface(mGothamBoldTypeface);

        viewHolder.mPriceView.setText(watchFace.getPrice() == 0 ? getString(R.string.free)
                : String.valueOf(watchFace.getPrice()) + DOLLAR_SIGN);
        viewHolder.mPriceView.setTypeface(mGothamBookTypeface);

        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intents.firePlayStoreIntent(getActivity(), watchFace.getPackage_name());
                AnalyticsHelper.recordItem(AnalyticsHelper.EVENT_ITEM_WATCH_FACE,
                        null,
                        watchFace.getName());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        String title = getString(R.string.app_name);
        if (actionBar != null) {
            SpannableString s = new SpannableString(title);
            s.setSpan(new TypefaceSpan(mGothamBookTypeface), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            actionBar.setTitle(s);
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null && mAdapter instanceof com.firebase.ui.database.FirebaseRecyclerAdapter) {
            ((com.firebase.ui.database.FirebaseRecyclerAdapter) mAdapter).cleanup();
        }
    }

    private void setGothamBookTypeface(Typeface gothamBookTypeface) {
        mGothamBookTypeface = gothamBookTypeface;
    }

    private void setGothamMediumTypeface(Typeface gothamMediumTypeface) {
        mGothamMediumTypeface = gothamMediumTypeface;
    }

    private void setGothamBoldTypeface(Typeface gothamBoldTypeface) {
        mGothamBoldTypeface = gothamBoldTypeface;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.customize_layout:
                Fragment customizeFragment = CustomizeFragment.newInstance(mGothamBookTypeface);
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.animator.slide_in_right,
                                R.animator.slide_out_left,
                                R.animator.slide_in_left,
                                R.animator.slide_out_right)
                        .replace(R.id.container, customizeFragment)
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.rate_layout:
                Intents.firePlayStoreIntent(getActivity(), getActivity().getPackageName());
                AnalyticsHelper.recordCustomEvent(AnalyticsHelper.EVENT_RATE);
                break;
            case R.id.facebook_layout:
                Intents.fireFacebookIntent(getActivity());
                AnalyticsHelper.recordCustomEvent(AnalyticsHelper.EVENT_CATEGORY_SOCIAL_MEDIA_TAPPED,
                        AnalyticsHelper.SOCIAL_MEDIA_TAPPED_FACEBOOK);
                break;
            case R.id.instagram_layout:
                Intents.fireInstagramIntent(getActivity());
                AnalyticsHelper.recordCustomEvent(AnalyticsHelper.EVENT_CATEGORY_SOCIAL_MEDIA_TAPPED,
                        AnalyticsHelper.SOCIAL_MEDIA_TAPPED_INSTAGRAM);
                break;
            case R.id.contact_layout:
                Intents.fireEmailIntent(getActivity(), "[" + getString(R.string.app_name) + "]");
                AnalyticsHelper.recordCustomEvent(AnalyticsHelper.EVENT_CONTACT_US);
                break;
        }
    }

    private void showMore(boolean show) {
        mDownloadMoreLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        mDownloadMoreDivider.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
