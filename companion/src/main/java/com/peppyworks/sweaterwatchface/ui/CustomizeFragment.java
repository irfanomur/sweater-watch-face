/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface.ui;

import com.peppyworks.sweaterwatchface.R;
import com.peppyworks.watchfaceconstructor.common.util.TypefaceSpan;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class CustomizeFragment extends Fragment implements View.OnClickListener {

    private Typeface mGothamBookTypeface;

    public CustomizeFragment() {
    }

    /**
     * Create a new instance of this fragment.
     */
    public static CustomizeFragment newInstance(Typeface gothamBook) {
        CustomizeFragment fragment = new CustomizeFragment();
        fragment.setGothamBookTypeface(gothamBook);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customize, container, false);
        TextView customizeText = (TextView) view.findViewById(R.id.customize_text);
        customizeText.setTypeface(mGothamBookTypeface);
        TextView timeFormatText = (TextView) view.findViewById(R.id.time_format_text);
        timeFormatText.setTypeface(mGothamBookTypeface);

        view.findViewById(R.id.ambient_mode).setOnClickListener(this);
        view.findViewById(R.id.time_format).setOnClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            SpannableString s = new SpannableString(getString(R.string.customize));
            s.setSpan(new TypefaceSpan(mGothamBookTypeface), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            actionBar.setTitle(s);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void setGothamBookTypeface(Typeface gothamBookTypeface) {
        mGothamBookTypeface = gothamBookTypeface;
    }

    @Override
    public void onClick(View v) {
        Fragment f = null;
        switch (v.getId()) {
            case R.id.ambient_mode:
                f = AmbientModeFragment.newInstance(mGothamBookTypeface);
                break;
            case R.id.time_format:
                f = TimeFormatFragment.newInstance(mGothamBookTypeface);
                break;
        }
        if (f != null) {
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.animator.slide_in_right,
                            R.animator.slide_out_left,
                            R.animator.slide_in_left,
                            R.animator.slide_out_right)
                    .replace(R.id.container, f)
                    .addToBackStack(null)
                    .commit();
        }
    }

}
