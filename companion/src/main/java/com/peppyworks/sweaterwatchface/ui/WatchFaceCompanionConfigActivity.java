/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface.ui;

import com.google.android.gms.wearable.DataMap;

import com.peppyworks.sweaterwatchface.R;
import com.peppyworks.watchfaceconstructor.common.util.WatchFaceUtil;
import com.peppyworks.watchfaceconstructor.mobile.CompanionConfigActivity;

import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

public class WatchFaceCompanionConfigActivity extends CompanionConfigActivity implements
        AmbientModeFragment.AmbientModeListener, TimeFormatFragment.TimeFormatListener  {

    private WatchFaceUtil.AmbientMode mCurrentAmbientMode = WatchFaceUtil.AmbientMode.DEFAULT;
    private WatchFaceUtil.TimeFormat mCurrentTimeFormat = WatchFaceUtil.TimeFormat.DEFAULT;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Typeface gothamMedium =
                Typeface.createFromAsset(getAssets(), getString(R.string.typeface_gotham_medium));
        Typeface gothamBold =
                Typeface.createFromAsset(getAssets(), getString(R.string.typeface_gotham_bold));
        Typeface gothamBook =
                Typeface.createFromAsset(getAssets(), getString(R.string.typeface_gotham_book));

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setBackgroundDrawable(
                    new ColorDrawable(getResources().getColor(R.color.peppy_primary_dark)));
        }

        if (savedInstanceState == null) {
            MainFragment mainFragment = MainFragment.newInstance(gothamBook,
                    gothamMedium,
                    gothamBold);
            getFragmentManager().beginTransaction()
                    .add(R.id.container, mainFragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDataMapFetched(DataMap dataMap) {
        if (dataMap != null) {
            if (dataMap.containsKey(WatchFaceUtil.KEY_AMBIENT_MODE)) {
                mCurrentAmbientMode = WatchFaceUtil.AmbientMode.valueOf(
                        dataMap.getString(WatchFaceUtil.KEY_AMBIENT_MODE));
            }
        }
    }

    @Override
    public void onAmbientModeChanged(WatchFaceUtil.AmbientMode ambientMode) {
        mCurrentAmbientMode = ambientMode;
        sendConfigUpdateMessage(WatchFaceUtil.KEY_AMBIENT_MODE, ambientMode.name());
    }

    @Override
    public void onTimeFormatChanged(WatchFaceUtil.TimeFormat timeFormat) {
        mCurrentTimeFormat = timeFormat;
        sendConfigUpdateMessage(WatchFaceUtil.KEY_TIME_FORMAT, timeFormat.name());
    }

    public WatchFaceUtil.AmbientMode getCurrentAmbientMode() {
        return mCurrentAmbientMode;
    }

    public WatchFaceUtil.TimeFormat getCurrentTimeFormat() {
        return mCurrentTimeFormat;
    }
}
