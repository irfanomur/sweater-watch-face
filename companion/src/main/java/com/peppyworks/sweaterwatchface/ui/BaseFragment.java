/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface.ui;

import com.peppyworks.sweaterwatchface.R;
import com.peppyworks.sweaterwatchface.ui.widget.SweaterWatchFaceView;
import com.peppyworks.watchfaceconstructor.common.WatchShape;
import com.peppyworks.watchfaceconstructor.common.util.TypefaceSpan;
import com.peppyworks.watchfaceconstructor.common.util.WatchFaceUtil;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public abstract class BaseFragment extends Fragment {

    private final String CONFIG_SHARED_PREFS = "CONFIG_SHARED_PREFS";
    private final String KEY_IS_ROUND = "KEY_IS_ROUND";

    private SharedPreferences mSharedPreferences;
    private boolean mIsRound = true;

    private Typeface mGothamBookTypeface;

    private SweaterWatchFaceView mWatchFaceView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPreferences =
                getActivity().getSharedPreferences(CONFIG_SHARED_PREFS, Context.MODE_PRIVATE);
        mIsRound = mSharedPreferences.getBoolean(KEY_IS_ROUND, true);

        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        String title = getTitle();
        if (actionBar != null) {
            SpannableString s = new SpannableString(title);
            s.setSpan(new TypefaceSpan(mGothamBookTypeface), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            actionBar.setTitle(s);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    void setWatchFaceView(SweaterWatchFaceView watchFaceView) {
        mWatchFaceView = watchFaceView;
        mWatchFaceView.setWatchShape(mIsRound ? WatchShape.CIRCLE : WatchShape.SQUARE);

        mWatchFaceView.setAmbientMode(getAmbientMode());
    }

    protected abstract String getTitle();

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_customize, menu);

        menu.findItem(R.id.round)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (!mIsRound) {
                            mIsRound = true;
                            SharedPreferences.Editor edit = mSharedPreferences.edit();
                            edit.putBoolean(KEY_IS_ROUND, true);
                            edit.apply();
                            mWatchFaceView.setWatchShape(WatchShape.CIRCLE);
                        }
                        return true;
                    }
                });

        menu.findItem(R.id.square)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (mIsRound) {
                            mIsRound = false;
                            SharedPreferences.Editor edit = mSharedPreferences.edit();
                            edit.putBoolean(KEY_IS_ROUND, false);
                            edit.apply();
                            mWatchFaceView.setWatchShape(WatchShape.SQUARE);
                        }
                        return true;
                    }
                });

    }

    Typeface getGothamBookTypeface() {
        return mGothamBookTypeface;
    }

    void setGothamBookTypeface(Typeface gothamBookTypeface) {
        mGothamBookTypeface = gothamBookTypeface;
    }

    WatchFaceUtil.AmbientMode getAmbientMode() {
        return ((WatchFaceCompanionConfigActivity) getActivity()).getCurrentAmbientMode();
    }

    WatchFaceUtil.TimeFormat getTimeFormat() {
        return ((WatchFaceCompanionConfigActivity) getActivity()).getCurrentTimeFormat();
    }

}
