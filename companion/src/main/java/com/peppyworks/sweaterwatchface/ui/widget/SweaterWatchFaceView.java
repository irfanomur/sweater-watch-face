/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface.ui.widget;

import com.peppyworks.sweaterwatchface.common.WatchFaceDrawer;
import com.peppyworks.watchfaceconstructor.mobile.WatchFaceView;

import android.content.Context;
import android.util.AttributeSet;

public class SweaterWatchFaceView extends WatchFaceView {
    private static final int DEFAULT_BATTERY_LEVEL = 50;

    public SweaterWatchFaceView(Context context) {
        super(context);
        init();
    }

    public SweaterWatchFaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SweaterWatchFaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        WatchFaceDrawer mWatchFaceDrawer = new WatchFaceDrawer(getContext());
        mWatchFaceDrawer.setMobilePreview(true);
        setPhoneBatteryLevel(DEFAULT_BATTERY_LEVEL);
        setWatchFaceDrawer(mWatchFaceDrawer);
    }
}
