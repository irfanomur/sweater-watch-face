/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface.ui;

import com.peppyworks.sweaterwatchface.R;
import com.peppyworks.sweaterwatchface.ui.widget.SweaterWatchFaceView;
import com.peppyworks.watchfaceconstructor.common.WatchMode;
import com.peppyworks.watchfaceconstructor.common.util.WatchFaceUtil;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class AmbientModeFragment extends BaseFragment implements
        RadioGroup.OnCheckedChangeListener {

    private AmbientModeListener mCallback;

    private SweaterWatchFaceView mWatchFaceView;

    public AmbientModeFragment() {
    }

    /**
     * Create a new instance of this fragment.
     */
    public static AmbientModeFragment newInstance(Typeface gothamBook) {
        AmbientModeFragment fragment = new AmbientModeFragment();
        fragment.setGothamBookTypeface(gothamBook);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ambient_mode, container, false);

        mWatchFaceView = (SweaterWatchFaceView) view.findViewById(R.id.watch_face_view);
        mWatchFaceView.setCurrentWatchMode(WatchMode.AMBIENT);
        setWatchFaceView(mWatchFaceView);

        RadioGroup modesContainer = (RadioGroup) view.findViewById(R.id.ambient_radio_group);
        modesContainer.setOnCheckedChangeListener(this);

        RadioButton blackWhiteRadioButton = (RadioButton) view.findViewById(R.id
                .select_black_white);
        blackWhiteRadioButton.setTypeface(getGothamBookTypeface());
        if (getAmbientMode().equals(WatchFaceUtil.AmbientMode.BLACK_WHITE) ||
                getAmbientMode().equals(WatchFaceUtil.AmbientMode.DEFAULT)) {
            blackWhiteRadioButton.setChecked(true);
        }

        RadioButton fullRadioButton = (RadioButton) view.findViewById(R.id.select_full);
        fullRadioButton.setTypeface(getGothamBookTypeface());
        if (getAmbientMode().equals(WatchFaceUtil.AmbientMode.FULL)) {
            fullRadioButton.setChecked(true);
        }

        RadioButton timeOnlyRadioButton = (RadioButton) view.findViewById(R.id.select_time);
        timeOnlyRadioButton.setTypeface(getGothamBookTypeface());
        if (getAmbientMode().equals(WatchFaceUtil.AmbientMode.TIME_ONLY)) {
            timeOnlyRadioButton.setChecked(true);
        }

        RadioButton digitalRadioButton = (RadioButton) view.findViewById(R.id.select_digital);
        digitalRadioButton.setTypeface(getGothamBookTypeface());
        if (getAmbientMode().equals(WatchFaceUtil.AmbientMode.DIGITAL_TIME)) {
            digitalRadioButton.setChecked(true);
        }

        RadioButton analogRadioButton = (RadioButton) view.findViewById(R.id.select_analog);
        analogRadioButton.setTypeface(getGothamBookTypeface());
        if (getAmbientMode().equals(WatchFaceUtil.AmbientMode.ANALOG_TIME)) {
            analogRadioButton.setChecked(true);
        }

        return view;
    }

    @Override
    protected String getTitle() {
        return getString(R.string.ambient_mode);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (AmbientModeListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + "must implement AmbientModeListener.");
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (mCallback != null) {
            WatchFaceUtil.AmbientMode ambientMode = null;
            switch (checkedId) {
                case R.id.select_black_white: {
                    ambientMode = WatchFaceUtil.AmbientMode.BLACK_WHITE;
                    break;
                }
                case R.id.select_full: {
                    ambientMode = WatchFaceUtil.AmbientMode.FULL;
                    break;
                }
                case R.id.select_time: {
                    ambientMode = WatchFaceUtil.AmbientMode.TIME_ONLY;
                    break;
                }
                case R.id.select_digital: {
                    ambientMode = WatchFaceUtil.AmbientMode.DIGITAL_TIME;
                    break;
                }
                case R.id.select_analog: {
                    ambientMode = WatchFaceUtil.AmbientMode.ANALOG_TIME;
                    break;
                }
            }
            if (ambientMode != null) {
                mWatchFaceView.setAmbientMode(ambientMode);
                mCallback.onAmbientModeChanged(ambientMode);
            }
        }
    }

    public interface AmbientModeListener {
        void onAmbientModeChanged(WatchFaceUtil.AmbientMode ambientMode);
    }
}
