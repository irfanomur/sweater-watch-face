/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface.ui;

import com.peppyworks.sweaterwatchface.R;
import com.peppyworks.sweaterwatchface.ui.widget.SweaterWatchFaceView;
import com.peppyworks.watchfaceconstructor.common.util.WatchFaceUtil;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

public class TimeFormatFragment extends BaseFragment {

    TimeFormatListener mCallback;

    private SweaterWatchFaceView mWatchFaceView;

    private SwitchCompat mUse24HourSwitch;
    private SwitchCompat mZeroPadHourSwitch;

    private boolean mIsInitialised = false;

    public TimeFormatFragment() {
    }

    /**
     * Create a new instance of this fragment.
     */
    public static TimeFormatFragment newInstance(Typeface gothamBook) {
        TimeFormatFragment fragment = new TimeFormatFragment();
        fragment.setGothamBookTypeface(gothamBook);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_time_format, container, false);

        mWatchFaceView = (SweaterWatchFaceView) view.findViewById(R.id.watch_face_view);
        setWatchFaceView(mWatchFaceView);

        mUse24HourSwitch = (SwitchCompat) view.findViewById(R.id.use_24_hour_switch);
        mUse24HourSwitch.setTypeface(getGothamBookTypeface());
        mUse24HourSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                updateFormat();
            }
        });

        mZeroPadHourSwitch = (SwitchCompat) view.findViewById(R.id
                .zero_pad_hour_switch);
        mZeroPadHourSwitch.setTypeface(getGothamBookTypeface());
        mZeroPadHourSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                updateFormat();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mUse24HourSwitch.setChecked(getTimeFormat().equals(WatchFaceUtil.TimeFormat.FORMAT_24) ||
                getTimeFormat().equals(WatchFaceUtil.TimeFormat.FORMAT_24_WITH_ZERO) ||
                (getTimeFormat().equals(WatchFaceUtil.TimeFormat.DEFAULT) &&
                        DateFormat.is24HourFormat(getActivity())));
        mZeroPadHourSwitch.setChecked(getTimeFormat().equals(WatchFaceUtil.TimeFormat.DEFAULT) ||
                getTimeFormat().equals(WatchFaceUtil.TimeFormat.FORMAT_12_WITH_ZERO)
                || getTimeFormat().equals(WatchFaceUtil.TimeFormat.FORMAT_24_WITH_ZERO));
        mIsInitialised = true;
        updateFormat();
    }

    @Override
    protected String getTitle() {
        return getString(R.string.time_format);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (TimeFormatListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + "must implement TimeFormatListener.");
        }
    }

    private void updateFormat() {
        if (mIsInitialised) {
            if (mUse24HourSwitch.isChecked() && mZeroPadHourSwitch.isChecked()) {
                mWatchFaceView.setTimeFormat(WatchFaceUtil.TimeFormat.FORMAT_24_WITH_ZERO);
                mCallback.onTimeFormatChanged(WatchFaceUtil.TimeFormat.FORMAT_24_WITH_ZERO);
            } else if (mUse24HourSwitch.isChecked()) {
                mWatchFaceView.setTimeFormat(WatchFaceUtil.TimeFormat.FORMAT_24);
                mCallback.onTimeFormatChanged(WatchFaceUtil.TimeFormat.FORMAT_24);
            } else if (mZeroPadHourSwitch.isChecked()) {
                mWatchFaceView.setTimeFormat(WatchFaceUtil.TimeFormat.FORMAT_12_WITH_ZERO);
                mCallback.onTimeFormatChanged(WatchFaceUtil.TimeFormat.FORMAT_12_WITH_ZERO);
            } else {
                mWatchFaceView.setTimeFormat(WatchFaceUtil.TimeFormat.FORMAT_12);
                mCallback.onTimeFormatChanged(WatchFaceUtil.TimeFormat.FORMAT_12);
            }
        }
    }

    public interface TimeFormatListener {
        void onTimeFormatChanged(WatchFaceUtil.TimeFormat timeFormat);
    }
}
