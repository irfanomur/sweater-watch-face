/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface.common;

import com.peppyworks.watchfaceconstructor.common.BaseWatchFaceDrawer;
import com.peppyworks.watchfaceconstructor.common.WatchMode;
import com.peppyworks.watchfaceconstructor.common.util.WatchFaceUtil;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class WatchFaceDrawer extends BaseWatchFaceDrawer {

    private Paint mXPaint = new Paint();

    private float mXSize;
    private float mXlineWidth;

    public WatchFaceDrawer(Context context) {
        super(context);

        mXPaint.setAntiAlias(true);
        mXPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    @Override
    public void onInitialised() {

        mXlineWidth = getFloatValueFromSpec(Spec.SPEC_mXLineWidth);
        mXPaint.setStrokeWidth(mXlineWidth);

        mXSize = (getWidth() - ((Sweater.sXColorMap[0].length - 1f) * mXlineWidth)) / Sweater.sXColorMap[0].length;

        onConfigChanged();
    }

    @Override
    public void clearConfig() {
    }

    @Override
    public void applyFullState() {
    }

    @Override
    public void applyBWState() {
    }

    @Override
    public void applyTimeOnlyState() {
    }

    @Override
    public void onDrawBW(Canvas canvas) {
        canvas.drawColor(Color.BLACK);

        for (int i = 0; i < Sweater.sXColorMap.length; i++) {
            for (int j = 0; j < Sweater.sXColorMap[i].length; j++) {
                if (Sweater.sXColorMap[i][j] == Sweater.XColor.X_COLOR_RED ||
                        Sweater.sXColorMap[i][j] == Sweater.XColor.X_COLOR_GREEN ||
                        Sweater.sXColorMap[i][j] == Sweater.XColor.X_COLOR_YELLOW) {
                    drawX(canvas, j, i, Sweater.XColor.X_COLOR_WHITE);
                }
            }
        }

        drawTime(canvas);
    }

    @Override
    public void onDrawFull(Canvas canvas) {
        canvas.drawColor(Spec.SPEC_COLOR_BG);

        for (int i = 0; i < Sweater.sXColorMap.length; i++) {
            for (int j = 0; j < Sweater.sXColorMap[i].length; j++) {
                drawX(canvas, j, i, Sweater.sXColorMap[i][j]);
            }
        }

        drawTime(canvas);
    }

    private void drawX(Canvas canvas, float x, float y, int color) {
        if (color != -1) {
            mXPaint.setColor(color);

            x = x * mXSize + x * mXlineWidth;
            y = y * mXSize + y * mXlineWidth;

            canvas.drawLine(x + mXSize, y, x, y + mXSize, mXPaint);
            canvas.drawLine(x, y, x + mXSize, y + mXSize, mXPaint);
        }
    }

    @Override
    public void onDrawTimeOnly(Canvas canvas) {
        canvas.drawColor(Color.BLACK);

        drawTime(canvas);
    }

    private void drawTime(Canvas canvas) {
        // Colon
        for (int[] colon : Sweater.sColonDots) {
            drawX(canvas, colon[0], colon[1], getCurrentWatchMode().equals(WatchMode.INTERACTIVE) ||
                    getWatchFaceCustomization().getAmbientMode().equals(WatchFaceUtil.AmbientMode.FULL) ?
                    Sweater.XColor.X_COLOR_YELLOW : Sweater.XColor.X_COLOR_WHITE);
        }

        // Hour
        String hour = getHourText();
        if (hour.length() == 2) {
            drawNumber(canvas, hour.charAt(0), 0);
            drawNumber(canvas, hour.charAt(1), 1);
        } else {
            drawNumber(canvas, ' ', 0);
            drawNumber(canvas, hour.charAt(0), 1);
        }

        // Minute
        String minute = getMinuteText();
        drawNumber(canvas, minute.charAt(0), 2);
        drawNumber(canvas, minute.charAt(1), 3);
    }

    private void drawNumber(Canvas canvas, char number, int position) {
        int[][] numberMap = Sweater.getNumber(number);
        for (int i = 0; i < numberMap.length; i++) {
            for (int j = 0; j < numberMap[i].length; j++) {
                int color = numberMap[i][j];
                if (!getCurrentWatchMode().equals(WatchMode.INTERACTIVE) &&
                        !getWatchFaceCustomization().getAmbientMode()
                                .equals(WatchFaceUtil.AmbientMode.FULL)) {
                    if (color == Sweater.XColor.X_COLOR_DEFAULT) {
                        continue;
                    }
                    color = Sweater.XColor.X_COLOR_WHITE;
                }
                drawX(canvas,
                        j + Sweater.sNumberPositions[position][0],
                        i + Sweater.sNumberPositions[position][1],
                        color);
            }
        }
    }

    /**
     * Container class for design specifications
     */
    private static class Spec {
        static final float SPEC_mXLineWidth = 3.0f;

        static final int SPEC_COLOR_BG = 0xFF0A2833;

    }
}
