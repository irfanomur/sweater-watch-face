/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.sweaterwatchface.common;

class Sweater {

    static final int[][] sXColorMap = new int[][]{
            {XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY}, // 1. line
            {XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED}, // 2. line
            {XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY}, // 3. line
            {XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED}, // 4. line
            {XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY}, // 5. line
            {XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED}, // 6. line
            {XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY}, // 7. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT}, // 8. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 9. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 10. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 11. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 12. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, -1,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 13. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 14. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 15. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, -1,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 16. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 17. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, -1, -1, -1,
                    -1, -1, XColor.X_COLOR_DEFAULT, -1,
                    -1, -1, -1, -1,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 18. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 19. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 20. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT}, // 21. line
            {XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED}, // 22. line
            {XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY,
                    XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY, XColor.X_COLOR_BURGUNDY}, // 23. line
            {XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED}, // 24. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT}, // 25. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT}, // 26. line
            {XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN}, // 27. line
            {XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN}, // 28. line
            {XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN}, // 29. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT}, // 30. line
            {XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_GREEN, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_GREEN, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE}, // 31. line
            {XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_DEFAULT, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_GREEN,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED}, // 32. line
            {XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE}, // 33. line
            {XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED}, // 34. line
            {XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE}, // 35. line
            {XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED}, // 36. line
            {XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE}, // 37. line
            {XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED,
                    XColor.X_COLOR_RED, XColor.X_COLOR_RED, XColor.X_COLOR_RED}, // 38. line
            {XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE,
                    XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE, XColor.X_COLOR_WHITE} // 39. line

    };

    private static final int[][] s0Map = new int[][]{
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //1. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //2. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //3. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //4. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //5. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //6. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //7. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //8. line
    };

    private static final int[][] s1Map = new int[][]{
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //1. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //2. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //3. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //4. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //5. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //6. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //7. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //8. line
    };

    private static final int[][] s2Map = new int[][]{
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //1. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //2. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //3. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //4. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //5. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //6. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //7. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_YELLOW}, //8. line
    };

    private static final int[][] s3Map = new int[][]{
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //1. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //2. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //3. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //4. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //5. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //6. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //7. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //8. line
    };

    private static final int[][] s4Map = new int[][]{
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //1. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //2. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //3. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //4. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_YELLOW}, //5. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //6. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //7. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //8. line
    };

    private static final int[][] s5Map = new int[][]{
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_YELLOW}, //1. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //2. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //3. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //4. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //5. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //6. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //7. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //8. line
    };

    private static final int[][] s6Map = new int[][]{
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //1. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //2. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //3. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //4. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //5. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //6. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //7. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //8. line
    };

    private static final int[][] s7Map = new int[][]{
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_YELLOW}, //1. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //2. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //3. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //4. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //5. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //6. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //7. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //8. line
    };

    private static final int[][] s8Map = new int[][]{
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //1. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //2. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //3. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //4. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //5. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //6. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //7. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //8. line
    };

    private static final int[][] s9Map = new int[][]{
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //1. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //2. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //3. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //4. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_YELLOW}, //5. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //6. line
            {XColor.X_COLOR_YELLOW, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_YELLOW}, //7. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW, XColor.X_COLOR_YELLOW,
                    XColor.X_COLOR_DEFAULT}, //8. line
    };

    private static final int[][] sNoNumberMap = new int[][]{
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //1. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //2. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //3. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //4. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //5. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //6. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //7. line
            {XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT, XColor.X_COLOR_DEFAULT,
                    XColor.X_COLOR_DEFAULT}, //8. line
    };

    static final int[][] sNumberPositions = new int[][]{
            {7, 10}, // first space
            {13, 10}, // second space
            {21, 10}, // third space
            {27, 10}, // fourth space
    };

    static final int[][] sColonDots = new int[][]{{19, 12}, {19, 15}};

    static int[][] getNumber(char number) {
        switch (Character.getNumericValue(number)) {
            case 0:
                return s0Map;
            case 1:
                return s1Map;
            case 2:
                return s2Map;
            case 3:
                return s3Map;
            case 4:
                return s4Map;
            case 5:
                return s5Map;
            case 6:
                return s6Map;
            case 7:
                return s7Map;
            case 8:
                return s8Map;
            case 9:
                return s9Map;
            default:
                return sNoNumberMap;
        }
    }

    /**
     * Container class for x colors
     */
    static class XColor {
        static final int X_COLOR_DEFAULT = 0xFF0B2F3F;
        static final int X_COLOR_BURGUNDY = 0xFF8D1C3A;
        static final int X_COLOR_RED = 0xFFCB3A37;
        static final int X_COLOR_YELLOW = 0xFFC58627;
        static final int X_COLOR_GREEN = 0xFF106F51;
        static final int X_COLOR_WHITE = 0xFFB3C1C4;
    }

}
